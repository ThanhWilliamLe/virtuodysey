﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sculp001 : MonoBehaviour
{
    public AudioRings ar;
    public MeshRenderer mr;
    public MeshRenderer topPiece;
    public MeshRenderer bottomPiece;
    public float slideTime = 1f;
    public float colorizeInterval = 0.25f;
    public Color topColor = new Color(0.95f, 0.45f, 0.85f, 0.85f);

    private void Start()
    {
        Lean.Touch.LeanTouch.OnFingerSwipe += Swipe;
    }

    private void OnDestroy()
    {
        Lean.Touch.LeanTouch.OnFingerSwipe -= Swipe;
    }

    void Swipe(Lean.Touch.LeanFinger lf)
    {
        Vector2 v = lf.SwipeScaledDelta;
        if (v.y > 0) SlideToTop();
        else if (v.y < 0) SlideToBottom();
    }

    void SlideToTop()
    {
        if (mr == null || topPiece == null) return;

        Vector3 v1 = mr.bounds.max;
        Vector3 v2 = topPiece.bounds.min;

        if (v1.y >= v2.y) return;

        StopAllCoroutines();
        StartCoroutine(Slide(new Vector3(0, v2.y - v1.y, 0), slideTime, () => StartCoroutine(TouchTop())));
    }

    void SlideToBottom()
    {
        if (mr == null || bottomPiece == null) return;

        Vector3 v1 = mr.bounds.min;
        Vector3 v2 = bottomPiece.bounds.max;

        if (v1.y <= v2.y) return;

        StopAllCoroutines();
        StartCoroutine(Slide(new Vector3(0, v2.y - v1.y, 0), slideTime, () => StartCoroutine(TouchBottom())));
    }

    IEnumerator Slide(Vector3 movement, float time, System.Action postSlide)
    {
        Vector3 v = mr.transform.position;
        Vector3 v2 = v + movement;
        float t = 0;
        while (t <= time)
        {
            float ratio = Mathf.Clamp01(t / time);
            ratio = Mathf.SmoothStep(0, 1, ratio);
            mr.transform.position = Vector3.Lerp(v, v2, ratio);

            t += Time.deltaTime;
            if (t > time) break;
            yield return null;
        }
        if (postSlide != null) postSlide();
    }

    IEnumerator TouchTop()
    {
        for (int i = ar.lrs.Count - 1; i >= 0; i--)
        {
            ar.SetRingColor(i, topColor);
            yield return new WaitForSeconds(colorizeInterval);
        }
    }

    IEnumerator TouchBottom()
    {
        for (int i = 0; i < ar.lrs.Count; i++)
        {
            ar.SetRingColor(i, ar.rings[i].color);
            yield return new WaitForSeconds(colorizeInterval);
        }
    }
}
