﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AudioRings : MonoBehaviour
{
    public MeshRenderer mr;

    [Header("Audio")]
    public AudioSource aus;
    [Range(0, 1)]
    public float waveTrim = 0.02f;
    public FFTWindow waveWindow;
    public Vector2 waveMagnitude = new Vector2(0.5f, 5f);
    public int volumeSamples = 4096;
    public float volumeMagnitude = 0.25f;

    [Header("Ring")]
    public GameObject ringPrefab;
    public float yPadding = 0.25f;
    public Vector2 ringScale = new Vector2(0.5f, 1f);

    [Header("Rings")]
    public List<AudioRingConfig> rings;
    public List<LineRenderer> lrs;

    private void Reset()
    {
        mr = GetComponentInParent<MeshRenderer>();
    }

    public Bounds Bounds
    {
        get { return mr.bounds; }
    }

    public int TotalPoints
    {
        get { return rings.Sum((r) => r.points); }
    }

    public float RingScale
    {
        get { return ringScale.y; }
    }

    // local space
    public Vector3 GetRingBaseCenter(int index)
    {
        Bounds b = Bounds;
        Vector3 v1 = transform.InverseTransformPoint(new Vector3(b.center.x, b.min.y, b.center.z));
        Vector3 v2 = transform.InverseTransformPoint(new Vector3(b.center.x, b.max.y, b.center.z));

        float x = v1.x;
        float z = v1.z;
        float y1 = v1.y + yPadding;
        float y2 = v2.y - yPadding;

        float r = rings.Count <= 1 ? 0.5f : (float)index / (rings.Count - 1);
        float y = Mathf.Lerp(y1, y2, r);

        return new Vector3(x, y, z);
    }

    // local space
    public Vector3 GetRingCenter(int index)
    {
        Vector3 c = GetRingBaseCenter(index);
        if (index >= 0 && index < rings.Count) c += rings[index].offset;
        return c;
    }

    private void Start()
    {
        MakeLineRenderers();
    }

    void MakeLineRenderers()
    {
        if (lrs == null) lrs = new List<LineRenderer>();
        foreach (LineRenderer lr in lrs)
        {
            Destroy(lr.gameObject);
        }
        lrs.Clear();

        for (int i = 0; i < rings.Count; i++)
        {
            AudioRingConfig ring = rings[i];
            Vector3 center = GetRingCenter(i);

            GameObject go = Instantiate(ringPrefab, transform);
            go.transform.localPosition = center;

            LineRenderer lr = go.GetComponent<LineRenderer>();
            lr.startColor = lr.endColor = ring.color;
            lr.positionCount = ring.points;

            lr.SetPositions(RingPositions(ring, 0));

            lrs.Add(lr);
        }
        SetRingsScale(RingScale);
    }

    public void SetRingsScale(float f)
    {
        for (int i = 0; i < lrs.Count; i++) SetRingScale(i, f);
    }

    public void SetRingScale(int i, float f)
    {
        if (i < 0 || i >= lrs.Count) return;
        lrs[i].transform.localScale = Vector3.one * f;
    }

    private void Update()
    {
        //RingFollowSpectrum();
        RingFollowVolume();
    }

    public void RingFollowSpectrum()
    {
        if (aus == null) return;

        int points = TotalPoints;
        float mag = Mathf.Lerp(waveMagnitude.x, waveMagnitude.y, transform.position.y);

        int samples = (int)(points / waveTrim);
        float power = Mathf.Log(samples, 2);
        power = Mathf.Clamp(Mathf.Round(power), 6, 13);
        samples = (int)Mathf.Pow(2, power);

        float[] vals0 = new float[samples];
        aus.GetSpectrumData(vals0, 0, waveWindow);
        float[] vals = new float[(int)(samples * waveTrim)];
        System.Array.Copy(vals0, vals, vals.Length);

        int pointCounter = 0;
        for (int i = 0; i < rings.Count; i++)
        {
            AudioRingConfig ring = rings[i];
            Vector3[] pos = RingPositions(ring, Time.time);
            for (int j = 0; j < pos.Length; j++)
            {
                int pointIndex = Mathf.FloorToInt(Mathf.Clamp01(pointCounter / (float)points) * vals.Length);
                float val = vals[pointIndex];
                pos[j].y += val * mag;

                pointCounter++;
            }
            lrs[i].SetPositions(pos);
        }
    }

    public Vector3[] RingPositions(AudioRingConfig ring, float time)
    {
        Vector3[] ps = new Vector3[ring.points];

        Vector3 spin = Quaternion.Euler(0, ring.spin * time, 0) * new Vector3(ring.radius, 0, 0);
        for (int j = 0; j < ring.points; j++)
        {
            Vector3 local = Quaternion.Euler(0, 360f / ring.points * j, 0) * spin;
            ps[j] = local;
        }

        return ps;
    }

    public void RingFollowVolume()
    {
        float[] rmss = GetRMS(lrs.Count);
        for (int i = 0; i < lrs.Count; i++)
        {
            SetRingScale(i, RingScale + rmss[i] * volumeMagnitude);
        }
    }

    float[] GetRMS(int count, int channel = 0)
    {
        float[] output = new float[volumeSamples];
        aus.GetOutputData(output, channel);

        float[] os = new float[count];
        int each = Mathf.FloorToInt(volumeSamples / (float)count);
        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < each; j++)
            {
                os[i] += output[Mathf.Clamp(i * each + j, 0, volumeSamples - 1)];
            }
            os[i] = Mathf.Sqrt(Mathf.Abs(os[i]) / each);
        }

        return os;
    }

    public void SetRingColor(int i, Color c)
    {
        if (i < 0 || i >= lrs.Count) return;
        lrs[i].startColor = lrs[i].endColor = c;
    }
}

[System.Serializable]
public class AudioRingConfig
{
    public int points = 32;
    public float radius = 5;
    public float spin = 0;
    public Vector3 offset;
    public Color color = Color.white;
}

#if UNITY_EDITOR
[CustomEditor(typeof(AudioRings))]
[CanEditMultipleObjects]
public class AudioRingsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();

        if (EditorGUI.EndChangeCheck())
        {
            AudioRings ar = target as AudioRings;
            ar.SetRingsScale(ar.RingScale);
        }
    }

    private void OnSceneGUI()
    {
        AudioRings ar = target as AudioRings;

        for (int i = 0; i < ar.rings.Count; i++)
        {
            AudioRingConfig ring = ar.rings[i];
            Handles.color = Color.white * 0.85f;
            Handles.zTest = UnityEngine.Rendering.CompareFunction.Always;

            Vector3 center = ar.GetRingCenter(i);
            if (Tools.current == Tool.View && Event.current.alt)
            {
                center = ar.transform.TransformPoint(center);
                center = Handles.PositionHandle(center, Quaternion.identity);
                center = ar.transform.InverseTransformPoint(center);
                ring.offset = center - ar.GetRingBaseCenter(i);
            }
            if (i < ar.rings.Count - 1)
            {
                Vector3 center2 = ar.GetRingCenter(i + 1);
                Handles.DrawLine(ar.transform.TransformPoint(center), ar.transform.TransformPoint(center2));
            }

            Vector3 ra0 = new Vector3(ring.radius, 0, 0);
            Vector3 ra = center + ra0;

            Handles.DrawLine(ar.transform.TransformPoint(center), ar.transform.TransformPoint(ra));
            ra = ar.transform.TransformPoint(ra);
            ra = Handles.FreeMoveHandle(ra,
                                        Quaternion.identity,
                                        0.1f,
                                        Vector3.one,
                                        Handles.DotHandleCap);
            ra = ar.transform.InverseTransformPoint(ra);
            ring.radius = Mathf.Abs(ra.x - center.x);

            Handles.color = ring.color;
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
            for (int j = 0; j < ring.points; j++)
            {
                Vector3 ra1 = Quaternion.Euler(0, 360f / ring.points * j, 0) * ra0;
                Vector3 ra2 = Quaternion.Euler(0, 360f / ring.points * (j + 1), 0) * ra0;

                ra1 = ar.transform.TransformPoint(ra1 * ar.RingScale + center);
                ra2 = ar.transform.TransformPoint(ra2 * ar.RingScale + center);

                Handles.DrawLine(ra1, ra2);
            }
        }
    }
}
#endif