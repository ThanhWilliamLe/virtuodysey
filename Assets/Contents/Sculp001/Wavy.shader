﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "TWLE/Wavy"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
        _Flatness ("Flatness", Range(0,1)) = 0
        _Brightness ("Brighness", Range(-1,1)) = 0
        _Contrast ("Contrast", Range(0,2)) = 1
		
		[Header(Wave direction)]
		[Toggle]_WaveFromX ("Wave from X", Float) = 1
		[Toggle]_WaveFromY ("Wave from Y", Float) = 1
		[Toggle]_WaveFromZ ("Wave from Z", Float) = 1

		[Header(Wave direction)]
		[Toggle]_WaveX ("Wave X", Float) = 1
		[Toggle]_WaveY ("Wave Y", Float) = 1
		[Toggle]_WaveZ ("Wave Z", Float) = 1

		[Header(Wave config)]
		_WaveLength ("Wave length", Float) = 1
		_WaveSpeed ("Wave speed", Float) = 1
		_WaveMagnitude ("Wave magnitude", Float) = 1

		[HideInInspector]
		_AudioVolume ("Audio volume", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Geometry" "LightMode"="ForwardBase"}

        LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 diffuse : COLOR0;
			};
			
            fixed4 _Color;
            float _Flatness;
            float _Brightness;
			float _Contrast;
			
			float _WaveFromX;
			float _WaveFromY;
			float _WaveFromZ;
			float _WaveX;
			float _WaveY;
			float _WaveZ;

			float _WaveLength;
			float _WaveSpeed;
			float _WaveMagnitude;
			float _AudioVolume;

			v2f vert (appdata v)
			{
				float waveR = v.vertex.x * _WaveFromX + v.vertex.y * _WaveFromY + v.vertex.z * _WaveFromZ;
				waveR = (waveR + _Time.y * _WaveSpeed) / _WaveLength;
				waveR = sin(waveR);

				float4 wave = float4(_WaveX, _WaveY, _WaveZ, 0) * waveR;
				v.vertex += wave * _WaveMagnitude * _AudioVolume;

				v2f o;				
				o.vertex = UnityObjectToClipPos(v.vertex);

                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diffuse = nl * _LightColor0;
                o.diffuse.rgb += ShadeSH9(half4(worldNormal,1));
				
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = _Color;
                fixed3 dif = col.rgb * i.diffuse;
                col.rgb = lerp(col.rgb, dif, 1-_Flatness);
                col.rgb = (col.rgb + (1,1,1) * _Brightness) * _Contrast;
				return col;
			}
			ENDCG
		}
	}
}
