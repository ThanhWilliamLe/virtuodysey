﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "TWLE/AudioLine"
{
	Properties
	{
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}

        LOD 100

        Blend SrcAlpha OneMinusSrcAlpha
      
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
			};
			
            fixed4 _Color;
            sampler2D _MainTex;

			v2f vert (appdata v)
			{            
				v2f o;				
				o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = v.texcoord;
                o.color = v.color * _Color;

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
                fixed4 color = tex2D (_MainTex, i.texcoord) * i.color;
				return color;
			}
			ENDCG
		}
	}
}
