﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "TWLE/Dory Blood"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_Color2 ("Color2", Color) = (1,1,1,1)
		_Color3 ("Color3", Color) = (1,1,1,1)
        _Flatness ("Flatness", Range(0,1)) = 0
        _Brightness ("Brighness", Range(-1,1)) = 0
        _Contrast ("Contrast", Range(0,2)) = 1
		
		_MoveSpeed ("Move speed", Float) = 2
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Geometry" "LightMode"="ForwardBase"}

        LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 diffuse : COLOR0;
				half3 worldNormal : TEXCOORD2;
				half3 normal : TEXCOORD3;
			};
			
            fixed4 _Color;
            fixed4 _Color2;
            fixed4 _Color3;
            float _Flatness;
            float _Brightness;
			float _Contrast;

			float _MoveSpeed;
			
			v2f vert (appdata v)
			{
				v2f o;				
				o.vertex = UnityObjectToClipPos(v.vertex);

                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diffuse = nl * _LightColor0;
                o.diffuse.rgb += ShadeSH9(half4(worldNormal,1));
				
				o.worldNormal = worldNormal;
				o.normal = v.normal;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 camForward = mul((float3x3)unity_CameraToWorld, float3(0,0,1));
				float edgeRatio = pow(1-abs(dot(i.worldNormal, camForward)), 0.8);
				float y = (i.normal.y + _Time.y * _MoveSpeed);
				y = abs((y % 1) - 0.5) * 2;

				fixed4 mainColor = _Color;
				fixed4 edgeColor = lerp(_Color2, _Color3, y);

				fixed4 col = lerp(mainColor, edgeColor, edgeRatio);

                fixed3 dif = col.rgb * i.diffuse;
                col.rgb = lerp(col.rgb, dif, 1-_Flatness);
                col.rgb = (col.rgb + (1,1,1) * _Brightness) * _Contrast;

				return col;
			}
			ENDCG
		}
	}
}
