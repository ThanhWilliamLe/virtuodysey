﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dory : MonoBehaviour
{
    public ParticleSystem ps;
    public AudioAffectedModel aam;
    public float audioThreshold = 0.2f;
    public int emit = 30;

    public float lastAudioVolume { get; private set; }

    void Update()
    {
        float vol = aam.GetRMS(0);
        if (lastAudioVolume < audioThreshold && vol >= audioThreshold)
        {
            DoParticles(emit);
            lastAudioVolume = vol;
        }
        else if (lastAudioVolume >= audioThreshold && vol < audioThreshold)
        {
            ClearParticles();
            lastAudioVolume = vol;
        }
    }

    void DoParticles(int count)
    {
        ps.randomSeed = (uint)Mathf.Abs(Random.Range(int.MinValue, int.MaxValue));
        ps.Clear();
        ps.Emit(count);
    }

    void ClearParticles()
    {
        ps.Clear();
    }
}
