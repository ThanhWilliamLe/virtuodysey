﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "TWLE/VertexHologram"
{
	Properties
	{
		//_MainTex ("Texture", 2D) = "white" {} 
		
		[Header(Base)]
		_Brightness ("Brighness", Range(-1,1)) = 0
		_AlphaMax ("Alpha max", Range(0,1)) = 0.75
		_AlphaMin ("Alpha min", Range(0,1)) = 0.1
		
		[Header(Colors)]
		_RimColor ("Rim Color", Color) = (1,1,1,1)
		_Color ("Line color", Color) = (1,1,1,1)
		
		[Header(Lines)]
		_LineGroupHeight ("Line group height", Float) = 1.5
		_LineThick ("Line thick", Float) = 0.1
		_LineThreshold ("Line threshold", Range(-1,1)) = 0
		_LineSpeed ("Line speed", Float) = 1

		[Header(Audio)]
		[NoScaleOffset] _AudioMap ("Audio map", 2D) = "black"{}
		_AudioMagnitude ("Audio magnitude", float) = 0.15
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "LightMode"="ForwardBase"}
		LOD 100
		
		Cull Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
                //float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
                //float2 uv : TEXCOORD0;
				fixed4 diffuse : COLOR0;
				float3 worldNormal : TEXCOORD2;
				float3 worldPos : TEXCOORD3;
			};
			
            //sampler2D _MainTex;
			float _Brightness;
			fixed _AlphaMax;
			fixed _AlphaMin;
			fixed4 _RimColor;
			fixed4 _Color;
			float _LineGroupHeight;
			float _LineThick;
			float _LineThreshold;
			float _LineSpeed;

			sampler2D _AudioMap;
			float _AudioMagnitude;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

                //o.uv = v.uv;

                o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul (unity_ObjectToWorld, v.vertex);

				float extrude = _AudioMagnitude * tex2Dlod( _AudioMap, float4(0, v.vertex.y,0,0) ).r ;
				float3 extrudeDir = o.worldNormal;
				extrudeDir.y = 0;
				o.vertex.xyz += extrudeDir * extrude;

				half nl = max(0, dot(o.worldNormal, _WorldSpaceLightPos0.xyz));
                o.diffuse = nl * _LightColor0;
                o.diffuse.rgb += ShadeSH9(half4(o.worldNormal,1));
				
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 camForward = mul((float3x3)unity_CameraToWorld, float3(0,0,1));
				fixed4 rimCol = _RimColor * pow(1-abs(dot(i.worldNormal, camForward)), 2);

				//fixed4 col = tex2D(_MainTex, i.uv) * i.diffuse * _Color;
				fixed4 col = i.diffuse * _Color;
				col.rgb += rimCol.rgb;
				col.rgb += fixed3(1,1,1) * _Brightness;

				float linePos = i.worldPos.y + _LineSpeed * _Time.y;

				float lineGroupRatio = (linePos % _LineGroupHeight) / _LineGroupHeight;
				float lineLocal = (linePos % _LineThick) / _LineThick;
				lineLocal =  1 - abs(0.5 - lineLocal)  * 4;
				float lineRatio = lineLocal > _LineThreshold ? 1 : 0;

				col.a = lerp(_AlphaMax, _AlphaMin, lineGroupRatio) * lineRatio;
				return col;
			}
			ENDCG
		}

	}

	Fallback "Mobile/Diffuse"
}
