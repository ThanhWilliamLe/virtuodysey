﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject target;

    // Update is called once per frame
    void Update()
    {
        float mult = Input.GetKey(KeyCode.LeftShift) ? 5 : 1;
        if (Input.GetKey(KeyCode.LeftControl))
        {
            Vector3 rot = Vector3.zero;
            if (Input.GetKey(KeyCode.W)) rot.x -= 1;
            if (Input.GetKey(KeyCode.A)) rot.y -= 1;
            if (Input.GetKey(KeyCode.S)) rot.x += 1;
            if (Input.GetKey(KeyCode.D)) rot.y += 1;
            if (Input.GetKey(KeyCode.E)) rot.z -= 1;
            if (Input.GetKey(KeyCode.Q)) rot.z += 1;
            transform.Rotate(rot * mult, Space.Self);
        }
        else
        {
            Vector3 move = Vector3.zero;
            if (Input.GetKey(KeyCode.W)) move.z += 1;
            if (Input.GetKey(KeyCode.A)) move.x -= 1;
            if (Input.GetKey(KeyCode.S)) move.z -= 1;
            if (Input.GetKey(KeyCode.D)) move.x += 1;
            if (Input.GetKey(KeyCode.R)) move.y += 1;
            if (Input.GetKey(KeyCode.F)) move.y -= 1;
            transform.position += transform.TransformDirection(move * 0.25f * mult);
        }

        if (target != null) transform.LookAt(target.transform.position);
    }
}
