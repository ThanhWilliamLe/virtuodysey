﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAffectedModel : MonoBehaviour
{
    public MeshRenderer mr;
    public AudioSource aus;
    public bool forceUpdateSamples;

    [Range(6, 13)]
    public int waveDetail = 8;
    [Range(0, 1)]
    public float waveTrim = 0.02f;
    [Range(0, 1)]
    public float waveThreshold = 0.15f;
    public int waveOutputSamples = 9192;
    public FFTWindow window;
    public Vector2 visualizeSize = new Vector2(10, 5);

    Texture2D tex;
    int sampleCount;
    int trimmedSampleCount;
    public float[] samples { get; private set; }
    public float[] trimmedSamples { get; private set; }

    public bool HasAudioMap
    {
        get { return mr!=null && mr.material.HasProperty("_AudioMap"); }
    }

    public bool HasAudioVolume
    {
        get { return mr != null && mr.material.HasProperty("_AudioVolume"); }
    }

    private void Reset()
    {
        mr = GetComponent<MeshRenderer>();
        aus = GetComponent<AudioSource>();
    }

    private void Start()
    {
        if (mr != null) mr.material = Instantiate(mr.material);
        Configurate();
    }

    [ContextMenu("Configurate")]
    public void Configurate()
    {
        if (!HasAudioMap && !forceUpdateSamples) return;

        sampleCount = (int)Mathf.Pow(2, waveDetail);
        trimmedSampleCount = (int)(sampleCount * waveTrim);

        samples = new float[sampleCount];
        trimmedSamples = new float[trimmedSampleCount];

        tex = new Texture2D(1, trimmedSampleCount);
        if (mr != null) mr.material.SetTexture("_AudioMap", tex);
    }

    private void Update()
    {
        if (HasAudioMap || forceUpdateSamples)
        {
            UpdateSamples();
            for (int i = 0; i < trimmedSamples.Length - 1; i++)
            {
                Vector3 v1 = new Vector3((float)i / trimmedSamples.Length * visualizeSize.x, trimmedSamples[i] * visualizeSize.y, 0);
                Vector3 v2 = new Vector3((float)(i + 1) / trimmedSamples.Length * visualizeSize.x, trimmedSamples[i + 1] * visualizeSize.y, 0);

                if (mr != null)
                {
                    v1 += mr.bounds.max;
                    v2 += mr.bounds.max;
                }

                Debug.DrawLine(v1, v2, Color.red);
            }
        }
    }

    void UpdateSamples()
    {
        if (!HasAudioMap && !forceUpdateSamples) return;

        aus.GetSpectrumData(samples, 0, window);
        for (int i = 0; i < trimmedSamples.Length; i++) trimmedSamples[i] = samples[i];
    }

    private void OnWillRenderObject()
    {
        UpdateAudioTexture();
        UpdateAudioVolume();
    }

    void UpdateAudioTexture()
    {
        if (!HasAudioMap) return;

        Color[] colors = new Color[trimmedSampleCount];
        for (int i = 0; i < colors.Length; i++)
        {
            float v = trimmedSamples[i];
            if (v < waveThreshold) v = 0;
            colors[i] = new Color(v, v, v, 1);
        }
        tex.SetPixels(colors);
        tex.Apply();
    }

    void UpdateAudioVolume()
    {
        if (!HasAudioVolume) return;

        float f = GetRMS();
        f = Mathf.Clamp01((f - waveThreshold) / (1 - waveThreshold));
        mr.material.SetFloat("_AudioVolume", f);
    }

    public float GetRMS(int channel = 0)
    {
        float[] output = new float[waveOutputSamples];
        aus.GetOutputData(output, channel);
        float s = 0;
        foreach (float f in output)
        {
            s += f * f;
        }
        s = s / waveOutputSamples;
        return Mathf.Sqrt(s);
    }
}
